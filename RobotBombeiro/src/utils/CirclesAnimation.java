/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Antonio
 */
public class CirclesAnimation extends JPanel implements ActionListener{

    boolean S1active, S2active, S3active, S4active, S5active, S6active, S7active, S8active;
    boolean S1_S2active, S1_S3active, S1_S4active, S1_S5active, S1_S6active, S1_S7active;//ligacoes apartir do 1
    boolean S2_S5active;//ligacoes apartir do 2
    boolean S3_S2active, S3_S5active;//ligacoes apartir do 3
    boolean S4_S2active, S4_S5active;//ligacoes apartir do 4
    boolean S5_S3active, S5_S4active, S5_S6active, S5_S7active;//ligacoes a partir do 5
    boolean S6_S2active, S6_S4active;//ligacoes a partir do 6
    boolean S7_S8active, S7_S6active, S7_S5active;//ligacoes a partir do 7
    
    Color activeColor = Color.BLUE;
    String text = "";
    boolean setup = true;
    JPanel jp;
    JLabel jl;
    long last_time = System.nanoTime();
    long timer = 0;
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(i == 0){
            i = 1;
        }
        else{
            i = 0;
        }
        
        repaint();
    }
    
    Timer tm = new Timer(1000, this);
    int i = 0;
    Font textFont = new Font("Arial", Font.BOLD, 25);
    
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
                
        drawLine(g, 20, 20, 175, 75, Color.black, S1_S2active); //S1 -> S2
        /*drawLine(g, 75, 325, 50, 75, Color.black, S1_S3active); //S1 -> S3 (Não utilizado)
        drawLine(g, 75, 325, 175, 225, Color.black, S1_S4active); //S1 -> S4 (Não utilizado)
        drawLine(g, 75, 325, 345, 150, Color.black, S1_S5active); //S1 -> S5 (Não utilizado)
        drawLine(g, 75, 325, 475, 150, Color.black, S1_S6active); //S1 -> S6 (Não utilizado)
        drawLine(g, 75, 325, 375, 325, Color.black, S1_S7active); //S1 -> S7 (Não utilizado)*/
        
        drawLine(g, 175, 75, 325, 150, Color.black, S2_S5active); //S2 -> S5
        
        //drawLine(g, 50, 75, 175, 75, Color.black, S3_S2active); //S3 -> S2 (Não utilizado)
        drawLine(g, 50, 140, 325, 165, Color.black, S3_S5active); //S3 -> S5
        
        //drawLine(g, 175, 225, 175, 75, Color.black, S4_S2active); //S4 -> S2 (Não utilizado)
        drawLine(g, 425, 69, 325, 159, Color.black, S4_S5active); //S4 -> S5
        
        drawLine(g, 325, 150, 50, 120, Color.black, S5_S3active); //S5 -> S3
        //drawLine(g, 320, 150, 175, 225, Color.black, S5_S4active); //S5 -> S4 (Não utilizado)
        //drawLine(g, 325, 150, 475, 150, Color.black, S5_S6active); //S5 -> S6 (Não utilizado)
        drawLine(g, 320, 150, 370, 325, Color.black, S5_S7active); //S5 -> S7
        
        //drawLine(g, 475, 150, 175, 75, Color.black, S6_S2active); //S6 -> S2 (Não utilizado)
        drawLine(g, 475, 150, 425, 69, Color.black, S6_S4active); //S6 -> S4
        
        drawLine(g, 375, 325, 525, 325, Color.black, S7_S8active); //S7 -> S8
        //drawLine(g, 385, 330, 335, 155, Color.black, S7_S5active); //S7 -> S5 (Não utilizado)
        drawLine(g, 375, 325, 500, 145, Color.black, S7_S6active); //S7 -> S6
               
        drawCircle(g, 10, 10, 50, Color.yellow, "S1", S1active);
        drawCircle(g, 150, 50, 50, Color.yellow, "S2", S2active);
        drawCircle(g, 25, 100, 50, Color.yellow, "S3", S3active);
        drawCircle(g, 400, 50, 50, Color.yellow, "S4", S4active);
        drawCircle(g, 300, 125, 50, Color.yellow, "S5", S5active);
        drawCircle(g, 450, 125, 50, Color.yellow, "S6", S6active);
        drawCircle(g, 350, 300, 50, Color.yellow, "S7", S7active);
        drawCircle(g, 500, 300, 50, Color.yellow, "S8", S8active);
        
        if(setup){
            jp = new JPanel();
            this.add(jp);
            jp.setOpaque(true);
            jp.setBackground(Color.white);
            jl = new JLabel(text);
            jp.add(jl);
            jl.setVisible(true);
            jp.setBounds(600, 0, 400, 400);
            repaint();
            setup = false;
        }
        
        jl.setText(text);
        jp.setBounds(600, 0, 400, 400);
        
        long time = System.nanoTime();
        int delta_time = (int) ((time - last_time) / 1000000);
        last_time = time;
        timer += delta_time;
        
        if(timer >= 0 && timer <2000){
            stateOne();
        }
        
        if(timer >= 2000 && timer <4000){
            transitionStateOneToStateTwo();
        }
        
        if(timer >= 4000 && timer <6000){
            stateTwo();
        }
        
        if(timer >= 6000 && timer <8000){ //pisca durante 2 segundos.
            transitionStateTwoToStateFive();
        }
        
        if(timer >=8000 && timer <10000){
            stateFive();
        }
        
        if(timer >=10000 && timer <12000){
            transitionStateFiveToStateThree();
        }
        
        if(timer >=12000 && timer <14000){
            stateThree();
        }
        
        if(timer>=14000 && timer <16000){
            transitionStateThreeToStateFive();
        }
        
        if(timer>=16000 && timer<18000){
            stateFive();
        }
        
        if(timer>=18000 && timer<20000){
            transitionStateFiveToStateThree();
        }
        
        if(timer>=20000 && timer<22000){
            stateThree();
        }
        
        if(timer>=22000 && timer<24000){
            transitionStateThreeToStateFive();
        }
        
        if(timer>=24000 && timer<26000){
            stateFive();
        }
        
        if(timer>=26000 && timer<28000){
            transitionStateFiveToStateSeven();
        }
        
        if(timer>=28000 && timer<30000){
            stateSeven();
        }
        
        if(timer>=30000 && timer<32000){
            transitionStateSevenToStateSix();
        }
        
        if(timer>=32000 && timer<34000){
            stateSix();
        }
        
        if(timer>=34000 && timer<36000){
            transitionStateSixToStateFour();
        }
        
        if(timer>=36000 && timer<38000){
            stateFour();
        }
        
        if(timer>=38000 && timer<40000){
            transitionStateFourToStateFive();
        }
        
        if(timer>=40000 && timer<42000){
            stateFive();
        }
        
        if(timer>=42000 && timer<44000){
           transitionStateFiveToStateThree();
        }
        
        if(timer>=44000 && timer<46000){
            stateThree();
        }

        if (timer >= 46000 && timer < 48000) {
            transitionStateThreeToStateFive();
        }

        if (timer >= 48000 && timer < 50000) {
            stateFive();
        }
        
        if(timer>=50000 && timer<52000){
         transitionStateFiveToStateThree();
        }
        
        if(timer>=52000 && timer<54000){
            stateThree();
        }
        
        if(timer>=54000 && timer<56000){
            transitionStateThreeToStateFive();
        }
        
        if(timer>=56000 && timer<58000){
         stateFive();
        }
        
        if(timer>=58000 && timer<60000){
           transitionStateFiveToStateSeven();
        }
        
        if(timer>=60000 && timer<62000){
           stateSeven();
        }
        
        if(timer>=62000 && timer<64000){
           transitionStateSevenToStateSix();
        }
        
        if(timer>=64000 && timer<66000){
          stateSix();
        }
        
        if(timer>=66000 && timer<68000){
            transitionStateSixToStateFour();
        }
        
        if(timer>=68000 && timer<70000){
            stateFour();
        }
        
        if(timer>=70000 && timer<72000){
            transitionStateFourToStateFive();
        }
        
        if(timer>=72000 && timer<74000){
            stateFive();
        }
        
        if(timer>=74000 && timer<76000){
            transitionStateFiveToStateThree();
        }
        
        if(timer>=76000 && timer<78000){
            stateThree();
        }
        
        if(timer>=78000 && timer<80000){
            transitionStateThreeToStateFive();
        }
        
        if(timer>=80000 && timer<82000){
            stateFive();
        }
        
        if(timer>=82000 && timer<84000){
            transitionStateFiveToStateSeven();
        }
        
        if(timer>=84000 && timer<86000){
            stateSeven();
        }
        
        if(timer>=86000 && timer<88000){
            transitionStateSevenToStateEight();
        }
        
        if(timer>=88000 && timer<90000){
            stateEight();
        }
        
        if(timer >= 90000){ //para o ciclo comecar de novo.
            timer = 0;
        }
        
        tm.start();
    }
    
    void stateOne(){ //s1 blinking
        S1active = true;
        S8active = false;
        text = "Robot está à espera de ativação";
    }
    
    void stateTwo(){ //seta do s1 para 2 para de piscar. S2 blinking
        S1_S2active = false;
        S3_S2active = false;
        S4_S2active = false;
        S6_S2active = false;
        S2active = true;
        text = "Robot tenta centrar-se";
    }
    
    void stateThree(){
        S1_S3active = false;
        S5_S3active = false;
        S3active = true;
        text = "Robot vira a direita";
    }
    
    void stateFour(){
        S1_S4active = false;
        S5_S4active = false;
        S6_S4active = false;
        S4active = true;
        text = "Robot vira a esquerda";
    }
    
    void stateFive(){
        S1_S5active = false;
        S2_S5active = false;
        S3_S5active = false;
        S4_S5active = false;
        S5active = true;
        text = "Robot a deslocar-se em frente";
    }
    
    void stateSix(){
        S1_S6active = false;
        S5_S6active = false;
        S7_S6active = false;
        S6active = true;
        text = "Robot inverte o sentido";
    }
    
    void stateSeven(){
        S1_S7active = false;
        S5_S7active = false;
        S7active = true;
        text = "Robot parado na entrada do quarto";
    }
    
    void stateEight(){
        S7_S8active = false;
        S8active = true;
        text = "Robot liga a ventoinha";
    }
    
    void transitionStateOneToStateTwo(){ //seta do s1 para s2 blinking. S1 para de piscar
        S1active = false;
        S1_S2active = true;
        text = "Robot é ativado";
    }
    
    void transitionStateTwoToStateFive(){ //s2 para de piscar. Seta de s2 para s5 blinking
        S2active = false;
        S2_S5active = true;
        text = "Robot encontra-se centrado e desloca-se em frente";
    }
    
    void transitionStateFiveToStateThree(){
        S5active = false;
        S5_S3active = true;
        text = "Sensor da direita deteta espaço livre para virar";
    }
    
    void transitionStateThreeToStateFive(){
        S3active = false;
        S3_S5active = true;
        text = "Sensor da frente deteta espaço livre a frente";
    }
    
    void transitionStateSixToStateTwo(){
        S6active = false;
        S6_S2active = true;
        text = "Distância às paredes da esquerda e direita igual. Robot tenta centrar-se";
    }
    
    void transitionStateFiveToStateSeven(){
        S6_S2active = false;
        S5active = false;
        S5_S7active = true;
        text = "Robot encontra-se a deslocar em frente";
    }
    
     void transitionStateSevenToStateFive(){
        S6_S2active = false;
        S5active = false;
        S7_S5active = true;
        text = "Robot encontra-se a deslocar em frente";
    }
    
    void transitionStateSevenToStateSix(){
        S7active = false;
        S7_S6active = true;
        text = "Robot não deteta vela";
    }
    
    void transitionStateSevenToStateEight(){
        S7active = false;
        S7_S8active = true;
        text = "Robot parado. Deteta a vela";
    }
    
    void transitionStateSixToStateFour(){
        S6active = false;
        S4active = false;
        S6_S4active = true;
        text = "Robo deteta parede a frente e a direita";
    }
    
    void transitionStateFourToStateFive(){
        S5active = false;
        S4active = false;
        S4_S5active = true;
        text = "Robo desloca-se em frente";
    }
    
    public void drawCircle(Graphics g, int x, int y, int size, Color color, String text, boolean active){
        if(!active || i == 1)
            g.setColor(color);
        else{
            g.setColor(activeColor);
        }
        g.fillArc(x, y, size, size, 0, 360);
        g.setColor(Color.BLACK);
        g.setFont(textFont);
        g.drawString(text, x+10, y+35);
    }
    
    public void drawLine(Graphics g, int xi, int yi, int xf, int yf, Color color, boolean active){
        if(!active || i == 1)
            g.setColor(color);
        else{
            g.setColor(activeColor);
        }
        g.drawLine(xi, yi, xf, yf);
        drawArrow(xi, yi, xf, yf, g);
    }
    
    void drawArrow(int x1, int y1,int x2,int y2,Graphics g)
    {

        int x[] = new int[3];
        int y[]={-10,0,10};
        
        if(x1 < x2){
            x[0] =  0;
            x[1] = 10;
            x[2] = 0;
        }else{
            x[0] =  0;
            x[1] = -10;
            x[2] = 0;
        }

     Graphics2D g2d = (Graphics2D) g;

     //will move the orgin
     int xDiff = Math.abs(x2-x1)/2;
     int yDiff = Math.abs(y2-y1)/2;
     int xMin, xMax, yMin, yMax;
     
     if(x1 < x2){
        xDiff += x1;
        xMin = x1;
        xMax = x2;
     }
     else{
        xDiff += x2;
        xMin = x2;
        xMax = x1;
     }
     
     if(y1 < y2){
        yDiff += y1;
        yMin = y1;
        yMax = y2;
     }
     else{
         yDiff +=y2;
         yMin = y2;
         yMax = y1;
     }
     
     g2d.translate(xDiff,yDiff);
     double angle = 0;
     angle=findLineAngle(x1,y1,x2,y2);
     g2d.rotate(angle);

    g2d.fillPolygon(new Polygon(x,y,3));
    
    ///will restore orgin
    g2d.rotate(-angle);
    g2d.translate(-xDiff,-yDiff);

    }
    
    private double findLineAngle(int x1, int y1,int x2,int y2)
    {
         if((x2-x1)==0)
         {
             if(y1 < y2){
                 return Math.toRadians(270);
             }else
                 return Math.toRadians(90);     
         }

         int deltaY = y2-y1;
         int deltaX = x2-x1;
         double math = deltaY/deltaX;
         return Math.atan(math);

    }

        /*public static void main(String[] args){
            
            CirclesAnimation ca = new CirclesAnimation();
            JFrame jf = new JFrame();
            jf.setTitle("Circles");
            jf.setSize(1000,400);
            jf.setVisible(true);
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jf.add(ca);
        }*/
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import com.mysql.jdbc.Connection;
import utils.Utils;
import controller.EditarElementoController;

/**
 *
 * @author Antonio
 */
public class EditarElementoUI {
    
    private final EditarElementoController controller;
    
    public EditarElementoUI(Connection connector)
    {
        controller = new EditarElementoController(connector);
    }
    
    public void IntroduzirDados()
    {
        /*Introdução de dados*/
        String nome = Utils.readLineFromConsole("Nome do elemento: ");
        int idade = Utils.IntFromConsole("Idade do elemento: ");
        int numero = Utils.IntFromConsole("Número do elemento: ");
        String escola = Utils.readLineFromConsole("Nome da escola do elemento: ");
        int NIF = Utils.IntFromConsole("NIF do elemento: ");
        
        String equipa;
        System.out.println("Pretende associar o elemento a uma equipa?");
        String extra=Utils.readLineFromConsole("Sim/Nao:");
        if(extra.equals("Nao")){
            equipa="0"; /*Elemento fica na equipa 0 na db. Como mandar valor nulo para futura alteracao?*/
        }else{
            equipa=Utils.readLineFromConsole("Insira o ID da Equipa a que pretende adicionar o elemento:");
        }
        
        controller.AdicionarElemento(nome, idade, numero, escola, NIF, equipa);
    }
    
    public void EliminarDados()
    {
        /*Eliminação de elemento através da primary key: NIF*/
        int NIF = Utils.IntFromConsole("NIF do elemento a remover: ");
        
        controller.EliminarElemento(NIF);
    }
    
    public void EditarDados()
    {
        //String opcao;
        System.out.println("Que elemento pretende editar?(introduza o NIF e Equipa)");
        int NIF_Original = Utils.IntFromConsole("NIF: ");
        String equipa_original= Utils.readLineFromConsole("Id da equipa");
        System.out.println("Introduza os dados: ");
        String nome = Utils.readLineFromConsole("Nome do elemento: ");
        int idade = Utils.IntFromConsole("Idade do elemento: ");
        int numero = Utils.IntFromConsole("Número do elemento: ");
        String escola = Utils.readLineFromConsole("Nome da escola do elemento: ");
        int NIF = Utils.IntFromConsole("NIF do elemento: ");
        String equipa = Utils.readLineFromConsole("ID da Equipa:");
        
        controller.EditarElemento(NIF, nome, idade, numero, escola, NIF_Original, equipa, equipa_original);
    }
    
    public void ConsultarDados_Esp()
    {
        String NIF1 = Utils.readLineFromConsole("Introduza ID do elemento que pretende consultar: ");
        controller.ConsultarDadosElemento_Esp(NIF1);
    
    }
    
    public void ConsultarDados()
    {
        controller.ConsultarDadosElemento();
    }
    
    public void ConsultarDadosMudancas(){
        controller.ConsultarMudancas();
    }
    
    public void run(){
        IntroduzirDados();
    }
    
    public void run_del()
    {
        EliminarDados();
    }
    
    public void run_edit(){
        EditarDados();
    }
    
    public void run_consult_esp(){
        ConsultarDados_Esp();
    }
    
    public void run_consult(){
        ConsultarDados();
    }
    
    public void run_consult_mudancas(){
        ConsultarDadosMudancas();
    }
}

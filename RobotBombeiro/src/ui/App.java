package ui;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Antonio
 */

import com.mysql.jdbc.Connection;
import utils.DatabaseConnection;

public class App {
    
    public static Connection conn;
    
    public static void main(String[] args) {
        
        try{
            conn = DatabaseConnection.getConnection();
            MenuUI menuUI = new MenuUI(conn);
            menuUI.run();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

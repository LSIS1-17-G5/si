/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import com.mysql.jdbc.Connection;
import controller.EditarRobotController;
import utils.Utils;

/**
 *
 * @author Antonio
 */
public class EditarRobotUI {
   
    private final EditarRobotController controller;
    
    public EditarRobotUI(Connection connector)
    {
        controller = new EditarRobotController(connector);
    }
   
    public void IntroduzirDados()
    {
        String ID = Utils.readLineFromConsole("ID do Robot: ");
        String ModoLigacao = Utils.readLineFromConsole("Modo de Ligação: ");
        String ModoApagar = Utils.readLineFromConsole("Modo de Extinção: ");
        double Comp = Utils.DoubleFromConsole("Comprimento: ");
        double Larg = Utils.DoubleFromConsole("Largura: ");
        double Alt = Utils.DoubleFromConsole("Altura: ");
        int NumSensores = Utils.IntFromConsole("Numero de Sensores presentes no Robot: ");
        String TipoSonar = Utils.readLineFromConsole("Modelo de Sonar: ");
        String TipoChama = Utils.readLineFromConsole("Modelo de Chama: ");
        double Tensao = Utils.DoubleFromConsole("Tensao do Robot: ");
        double Corrente = Utils.DoubleFromConsole("Corrente do robot: ");
        
        String equipa;
        System.out.println("Pretende associar o robot a uma equipa?");
        String extra=Utils.readLineFromConsole("Sim/Nao:");
        if(extra.equals("Nao")){
            equipa="0"; /*Robot fica na equipa 0 na db. Como mandar valor nulo para futura alteracao?*/
        }else{
            equipa=Utils.readLineFromConsole("Insira o ID da Equipa a que pretende adicionar o robot:");
        }
        controller.AdicionarRobot(ID, ModoLigacao, ModoApagar, Comp, Larg, Alt, NumSensores, TipoSonar, TipoChama, Tensao, Corrente, equipa);
    
    }
    
    public void EliminarDados()
    {
        String ID=Utils.readLineFromConsole("ID do robot a remover: ");
        controller.EliminarRobot(ID);
    
    }
    
    public void EditarDados()
    {
        System.out.println("Que robot pretende editar?(introduza o ID) ");
        String ID_Original=Utils.readLineFromConsole("ID do Robot a editar: ");
        System.out.println("Introduza os dados que pretende editar: ");
        String ID = Utils.readLineFromConsole("ID do Robot: ");
        String ModoLigacao = Utils.readLineFromConsole("Modo de Ligação: ");
        String ModoApagar = Utils.readLineFromConsole("Modo de Extinção: ");
        double Comp = Utils.DoubleFromConsole("Comprimento: ");
        double Larg = Utils.DoubleFromConsole("Largura: ");
        double Alt = Utils.DoubleFromConsole("Altura: ");
        int NumSensores = Utils.IntFromConsole("Numero de Sensores presentes no Robot: ");
        String TipoSonar = Utils.readLineFromConsole("Modelo de Sonar: ");
        String TipoChama = Utils.readLineFromConsole("Modelo de Chama: ");
        double Tensao = Utils.DoubleFromConsole("Tensao do Robot: ");
        double Corrente = Utils.DoubleFromConsole("Corrente do robot: ");
        String equipa=Utils.readLineFromConsole("ID da Equipa: ");
        
        controller.EditarRobot(ID, ModoLigacao, ModoApagar, Comp, Larg, Alt, NumSensores, TipoSonar, TipoChama, Tensao, Corrente, ID_Original, equipa);
    
    }
    
    public void ConsultarDados()
    {
        String ID_Bot = Utils.readLineFromConsole("Introduza ID do robot que pretende consultar: ");
        controller.ConsultarDadosRobot(ID_Bot);
    
    }

    public void ConsultarDados_Esp()
    {
        controller.ConsultarDadosRobot_Esp();
    }
    public void run(){
        IntroduzirDados();
    }
    
    public void run_del()
    {
        EliminarDados();
    }
    
    public void run_edit(){
        EditarDados();
    }
    
    public void run_consult(){
        ConsultarDados();
    }
    
    public void run_consult_esp()
    {
        ConsultarDados_Esp();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import com.mysql.jdbc.Connection;
import controller.EditarProvaController;
import java.util.Date;
import utils.Utils;

/**
 *
 * @author Antonio
 */
public class EditarProvaUI {
    private final EditarProvaController controller;
    
    public EditarProvaUI(Connection connector)
    {
        controller = new EditarProvaController(connector);
    }
    
     public void IntroduzirDados()
    {
        /*Introdução de dados*/
        String codigo = Utils.readLineFromConsole("Código da prova: ");
        String DataHora = Utils.readLineFromConsole("Data da prova: (DDDD-MMMM-AAAA HH:MM)");
        //int Classificacao = Utils.IntFromConsole("Classificacao da prova: ");
        
        String equipa;
        System.out.println("Pretende associar a prova a uma equipa?");
        String extra=Utils.readLineFromConsole("Sim/Nao:");
        if(extra.equals("Nao")){
            equipa="0"; /*Elemento fica na equipa 0 na db. Como mandar valor nulo para futura alteracao?*/
        }else{
            equipa=Utils.readLineFromConsole("Insira o ID da Equipa a que pretende adicionar a prova:");
        }
        controller.AdicionarProva(codigo, DataHora, /*Classificacao,*/ equipa);
    }
    
    public void EliminarDados()
    {
        /*Eliminação de elemento através da primary key: NIF*/
        String codigo = Utils.readLineFromConsole("Codigo da prova original: ");
        
        controller.EliminarProva(codigo);
    }
    
    public void EditarDados()
    {
        //String opcao;
        System.out.println("Que prova pretende editar?(introduza a data)");
        String codigo_Original = Utils.readLineFromConsole("Codigo da prova original:" );
        System.out.println("Introduza os dados: ");
        String codigo = Utils.readLineFromConsole("Codigo da prova: ");
        String DataHora = Utils.readLineFromConsole("Data da prova (DDDD-MMMM-AAAA HH:MM): ");
        //int Classificacao = Utils.IntFromConsole("Classificação da prova: ");
        String equipa=Utils.readLineFromConsole("Equipa a fazer aprova: ");
        controller.EditarProva(codigo, DataHora, /*Classificacao,*/ codigo_Original, equipa);
    }
    public void ConsultarDados()
    {
        String Codigo = Utils.readLineFromConsole("Introduza codigo de prova a consultar");
        controller.ConsultarDadosProva(Codigo);
    
    }
    
    public void ConsultarDados_Esp()
    {
        controller.ConsultarDadosProva_Esp();
    }
    
    public void run(){
        IntroduzirDados();
    }
    
    public void run_del()
    {
        EliminarDados();
    }
    
    public void run_edit(){
        EditarDados();
    }
    
    public void run_consult(){
        ConsultarDados();
    }
    
    public void run_consult_esp()
    {
        controller.ConsultarDadosProva_Esp();
    }
}

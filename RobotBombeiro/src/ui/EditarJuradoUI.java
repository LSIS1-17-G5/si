/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import com.mysql.jdbc.Connection;
import utils.Utils;
import controller.EditarJuradoController;

/**
 *
 * @author Antonio
 */
public class EditarJuradoUI {
    
    private final EditarJuradoController controller;
    
    public EditarJuradoUI(Connection connector)
    {
        controller = new EditarJuradoController(connector);
    }
    
    
    public void IntroduzirDados()
    {
        /*Introdução de dados*/
        String CodigoJurado = Utils.readLineFromConsole("ID do Jurado: ");
        String Nome = Utils.readLineFromConsole("Nome do Jurado: ");
        String Email = Utils.readLineFromConsole("Email do Jurado: ");
        int Idade = Utils.IntFromConsole("Idade do Jurado: ");
        
        
        String ID_Juri;
        System.out.println("Pretende associar o jurado a um júri?");
        String extra=Utils.readLineFromConsole("Sim/Nao:");
        if(extra.equals("Nao")){
            ID_Juri="JUR0"; /*Jurado fica na equipa 0 na db. Como mandar valor nulo para futura alteracao?*/
        }else{
            ID_Juri=Utils.readLineFromConsole("Insira o ID do Júri a que pretende adicionar o jurado:");
        }
        
        controller.AdicionarJurado(CodigoJurado, Nome, Email, Idade, ID_Juri);
    }
    
    public void EliminarDados()
    {
        /*Eliminação de elemento através da primary key: NIF*/
        String CodigoJurado = Utils.readLineFromConsole("ID do Jurado a eliminar: ");
        
        controller.EliminarJurado(CodigoJurado);
    }
    
    public void EditarDados()
    {
        //String opcao;
        System.out.println("Que jurado pretende editar?(introduza ID)");
        String CodigoJurado_Original = Utils.readLineFromConsole("ID do Jurado Original: ");
        System.out.println("Introduza os dados: ");
        String CodigoJurado = Utils.readLineFromConsole("ID do Jurado: ");
        String Nome = Utils.readLineFromConsole("Nome do Jurado: ");
        String Email = Utils.readLineFromConsole("Email do Jurado: ");
        int Idade = Utils.IntFromConsole("Idade do Jurado: ");
        String ID_Juri=Utils.readLineFromConsole("Insira o ID do Júri a que pretende adicionar o jurado (JUR0 default):");
        
        controller.EditarJurado(CodigoJurado, Nome, Email, Idade, ID_Juri, CodigoJurado_Original);
    }
    
    public void ConsultarDados_Esp()
    {
        String CodigoJurado1 = Utils.readLineFromConsole("Introduza ID do jurado que pretende consultar: ");
        controller.ConsultarDadosJurado_Esp(CodigoJurado1);
    
    }
    
    public void ConsultarDados()
    {
        controller.ConsultarDadosJurado();
    }
    
    public void run(){
        IntroduzirDados();
    }
    
    public void run_del()
    {
        EliminarDados();
    }
    
    public void run_edit(){
        EditarDados();
    }
    
    public void run_consult_esp(){
        ConsultarDados_Esp();
    }
    
    public void run_consult(){
        ConsultarDados();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import com.mysql.jdbc.Connection;
import utils.Utils;
import controller.EditarClassificacaoController;

/**
 *
 * @author Antonio
 */
public class EditarClassificacaoUI {
    private final EditarClassificacaoController controller;
    
    public EditarClassificacaoUI(Connection connector)
    {
        controller = new EditarClassificacaoController(connector);
    }
    
    public void IntroduzirDados()
    {
        String ID_Juri=Utils.readLineFromConsole("ID Juri a avaliar: ");
        String ID_Prova=Utils.readLineFromConsole("Prova a ser avaliada: ");
        String ID_Classificacao=Utils.readLineFromConsole("ID Classificação: ");
        int Class_Content=Utils.IntFromConsole("Avaliação da componente Content: ");
        int Class_Visual=Utils.IntFromConsole("Avaliação da componente Visual: ");
        int Class_Presentation=Utils.IntFromConsole("Avaliação da componente Presentation: ");
        
        int Class_Total=(int) ((Class_Content*0.4)+(Class_Visual*0.3)+(Class_Presentation*0.3));
        System.out.println("A Classificação final foi de "+Class_Total);
    
        controller.AdicionarClassificacao(ID_Classificacao, Class_Content, Class_Visual, Class_Presentation, Class_Total, ID_Prova, ID_Juri);
    }
    
    public void EliminarDados()
    {
    String ID_Classificacao=Utils.readLineFromConsole("Classificacao a ser eliminada: ");
    
    controller.EliminarClassificacao(ID_Classificacao);
    }

    public void EditarDados()
    {
        System.out.println("Que classificacao pretende editar?(introduza o ID da classificacao) ");
        String ID_Classificacao_Original=Utils.readLineFromConsole("ID Classificação Original: ");
        String ID_Juri=Utils.readLineFromConsole("ID do Juri a avaliar: ");
        String ID_Prova=Utils.readLineFromConsole("Prova a ser avaliada: ");
        String ID_Classificacao=Utils.readLineFromConsole("ID Classificação: ");
        int Class_Content=Utils.IntFromConsole("Avaliação da componente Content: ");
        int Class_Visual=Utils.IntFromConsole("Avaliação da componente Visual: ");
        int Class_Presentation=Utils.IntFromConsole("Avaliação da componente Presentation: ");
        int Class_Total=(int) ((Class_Content*0.4)+(Class_Visual*0.3)+(Class_Presentation*0.3));
        System.out.println("A Classificação final foi de "+Class_Total);
        
        controller.EditarClassificacao(ID_Classificacao, Class_Content, Class_Visual, Class_Presentation, Class_Total, ID_Prova, ID_Juri, ID_Classificacao_Original);
    
    }

     public void ConsultarDados_Esp()
    {
        String Codigo = Utils.readLineFromConsole("Introduza codigo de prova a consultar");
        controller.ConsultarDadosClassificacao_Esp(Codigo);
    
    }
    
    public void ConsultarDados()
    {
        controller.ConsultarDadosClassificacao();
    }
    
     public void run(){
        IntroduzirDados();
    }
    
    public void run_del()
    {
        EliminarDados();
    }
    
    public void run_edit(){
        EditarDados();
    }
    
    public void run_consult_esp(){
        ConsultarDados_Esp();
    }
    
    public void run_consult(){
        ConsultarDados();
    }

}

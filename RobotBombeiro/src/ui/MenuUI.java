/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import utils.CirclesAnimation;
import utils.Utils;

/**
 *
 * @author Antonio
 */
public class MenuUI {
    
    private String opcao;
    private String opcaoInterna;
    private static Connection conn;

    public MenuUI(Connection connection)
    {
        conn = connection;
    }

    public void run() throws IOException, SQLException
    {
        do
        {
            //opcao = "1";
            System.out.println("\n\n");
            System.out.println("1. Editar elementos");
            System.out.println("2. Editar equipas");
            System.out.println("3. Editar provas");
            System.out.println("4. Editar robots");
            System.out.println("5. Editar jurado");
            System.out.println("6. Editar juri");
            System.out.println("7. Consultar dados de Robot");
            System.out.println("8. Classificar prova");
            System.out.println("9. Consultar data de prova");
            System.out.println("10. Consultar dados de classificacao");
            System.out.println("11. Consultar tabela de mudanças");
            System.out.println("12. Animação");
//            System.out.println("5. Definir Serviço");
//            System.out.println("6. Efetuar Previsão de Produção");
//            System.out.println("7. Especificar Preço de Venda");
//            System.out.println("8. Especificar Transporte");
//            System.out.println("9. Atualizar Informação de Colaboradores");
//            System.out.println("10. Processar Informação");
            
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if(opcao.equals("1"))
            {
                EditarElementoUI ui = new EditarElementoUI(conn);
                System.out.println("\n*****DADOS DE ELEMENTOS*****");
                ui.run_consult();
                System.out.println("****************************\n");
                
                do{
                    System.out.println("1. Adicionar Elemento");
                    System.out.println("2. Eliminar Elemento");
                    System.out.println("3. Editar Elemento");
                    System.out.println("0. Sair");

                    opcaoInterna = Utils.readLineFromConsole("Introduza opção: ");
                    
                    if(opcaoInterna.equals("1")){
                        
                        ui.run();
                    }
                    
                    if(opcaoInterna.equals("2")){

                        ui.run_del();
                    }
                    
                    if(opcaoInterna.equals("3")){                   
                        
                        ui.run_consult_esp();/*apresenta lista de elementos para editar*/
                        ui.run_edit();
                    }
                    
                }while(!opcaoInterna.equals("0"));
                
            }
            
            if(opcao.equals("2")){
                do{
                    System.out.println("*****DADOS DE EQUIPAS*****");
                    EditarEquipaUI ui = new EditarEquipaUI(conn);
                    ui.run_consult();
                    System.out.println("**************************");
                    System.out.println("1. Adicionar Equipa");
                    System.out.println("2. Eliminar Equipa");
                    System.out.println("3. Editar Equipa");
                    
                    System.out.println("0. Sair");
                    
                    
                    
                    opcaoInterna=Utils.readLineFromConsole("Introduza a Opcao");
                    
                    if(opcaoInterna.equals("1")){
                        ui.run();
                    }
                    if(opcaoInterna.equals("2")){
                        
                        EditarElementoUI ui2 = new EditarElementoUI(conn);
                        EditarProvaUI ui3 = new EditarProvaUI(conn);
                        EditarRobotUI ui4 = new EditarRobotUI(conn);
                        EditarClassificacaoUI ui5 = new EditarClassificacaoUI(conn);
                        System.out.println("\n******DADOS DE EQUIPA******");
                        ui.run_consult();
                        System.out.println("***************************\n");
                        System.out.println("\n*****DADOS DE ELEMENTOS****");
                        ui2.run_consult();
                        System.out.println("***************************\n");
                        System.out.println("\n******DADOS DE PROVA*******");
                        ui3.run_consult_esp();
                        System.out.println("***************************\n");
                        System.out.println("\n******DADOS DE ROBOT*******");
                        ui4.run_consult_esp();
                        System.out.println("***************************\n");
                        System.out.println("\n**DADOS DE CLASSIFICAÇÕES**");
                        ui5.run_consult();
                        System.out.println("***************************\n");
                        
                        String continuar;
                        System.out.println("**********Retire os elementos, provas e robots da equipa a eliminar********");
                        String TerProva = Utils.readLineFromConsole("A equipa já tem uma prova associada? (Sim/Nao)");
                        if(TerProva.equals("Sim")){
                            String TerClassificacao = Utils.readLineFromConsole("A prova tem classificacao associada? (Sim/Nao)");
                            if(TerClassificacao.equals("Sim")){
                                ui5.run_del();
                            }
                            ui3.run_del();
                        }
                        
                        String TerRobot = Utils.readLineFromConsole("A equipa já tem um robot associado? (Sim/Nao)");
                        if(TerRobot.equals("Sim")){
                            ui4.run_del();
                        }
                        
                        String TerElemento = Utils.readLineFromConsole("A equipa tem elementos associados? (Sim/Nao)");
                        
                        if(TerElemento.equals("Sim")){
                            do{
                                ui2.run_del();
                                System.out.println("Eliminar mais elementos?");
                                continuar = Utils.readLineFromConsole("Sim/Nao");
                            }while(!continuar.equals("Nao"));
                        }
                        ui.run_del();
                        
                    }
                    
                    if(opcaoInterna.equals("3")){
                        System.out.println("\n******DADOS DE EQUIPA******");
                        ui.run_consult_esp();
                        System.out.println("*****************************\n");
                        ui.run_edit();
                    }
            }while(!opcaoInterna.equals("0"));
            }
            
            if(opcao.equals("3")){
                do{
                    System.out.println("\n*****DADOS DE PROVAS******");
                    EditarProvaUI ui = new EditarProvaUI(conn);
                    ui.run_consult_esp();
                    System.out.println("***************************\n");
                    System.out.println("1. Adicionar Prova");
                    System.out.println("2. Eliminar Prova");
                    System.out.println("3. Editar Prova");
                    
                    System.out.println("0. Sair");
                    
                    opcaoInterna=Utils.readLineFromConsole("Introduza a Opcao");
                    
                    if(opcaoInterna.equals("1")){
                        System.out.println("\n*****DADOS DE PROVAS******");
                        ui.run_consult_esp();
                        System.out.println("***************************\n");
                        ui.run();
                    }
                    if(opcaoInterna.equals("2")){
                        EditarClassificacaoUI ui2 = new EditarClassificacaoUI(conn);
                        System.out.println("\n*****DADOS DE PROVAS******");
                        ui.run_consult_esp();
                        System.out.println("***************************\n");
                        
                        String TerClassificacao = Utils.readLineFromConsole("A prova tem classificacao associada? (Sim/Nao");
                        if(TerClassificacao.equals("Sim")){
                            System.out.println("\n*****DADOS DE CLASSIFICACAO******");
                            ui2.run_consult();
                            System.out.println("**********************************\n");
                            
                            ui2.run_del();
                        }
                        ui.run_del();
                    }
                    if(opcaoInterna.equals("3")){
                        System.out.println("\n*****DADOS DE PROVAS******");
                        ui.run_consult_esp();
                        System.out.println("****************************\n");
                        ui.run_edit();
                    }
            }while(!opcaoInterna.equals("0"));
            }
            
            if(opcao.equals("4")){
                do{
                    EditarRobotUI ui = new EditarRobotUI(conn);
                    System.out.println("\n******DADOS DO ROBOT******");
                    ui.run_consult_esp();
                    System.out.println("**************************\n");
                    
                    System.out.println("1. Adicionar Robot");
                    System.out.println("2. Eliminar Robot");
                    System.out.println("3. Editar Robot");
                    
                    System.out.println("0. Sair");
                    
                    opcaoInterna=Utils.readLineFromConsole("Introduza a Opcao");
                    
                    if(opcaoInterna.equals("1")){
                        
                        ui.run();
                    }
                    if(opcaoInterna.equals("2")){
                        System.out.println("\n******DADOS DO ROBOT******");
                        ui.run_consult_esp();
                        System.out.println("**************************\n");
                        ui.run_del();
                    }
                    if(opcaoInterna.equals("3")){
                        
                        ui.run_consult();
                        ui.run_edit();
                    }
            }while(!opcaoInterna.equals("0"));
            }
            // Incluir as restantes opções aqui
            if(opcao.equals("8")){
                do{
                    
                    EditarClassificacaoUI ui = new EditarClassificacaoUI(conn);
                    System.out.println("\n**********DADOS DE CLASSIFICACAO**********");
                    ui.run_consult();
                    System.out.println("******************************************\n");
                    
                    System.out.println("1. Adicionar Classificação");
                    System.out.println("2. Eliminar Classificação");
                    System.out.println("3. Editar Classificação");
                    
                    System.out.println("0. Sair");
                    
                    opcaoInterna=Utils.readLineFromConsole("Introduza a Opcao");
                    
                    if(opcaoInterna.equals("1")){
                    
                        ui.run();
                    }
                    if(opcaoInterna.equals("2")){
                        System.out.println("\n**********DADOS DE CLASSIFICACAO**********");
                        ui.run_consult();
                        System.out.println("******************************************\n");
                        ui.run_del();
                    }
                    if(opcaoInterna.equals("3")){
                        
                        ui.run_consult_esp();
                        ui.run_edit();
                    }
            }while(!opcaoInterna.equals("0"));
            }
            
            if(opcao.equals("5"))
            {
                do{
                    EditarJuradoUI ui = new EditarJuradoUI(conn);
                    System.out.println("\n**********DADOS DE JURADO**********");
                    ui.run_consult();
                    System.out.println("***********************************\n");
                    System.out.println("1. Adicionar Jurado");
                    System.out.println("2. Eliminar Jurado");
                    System.out.println("3. Editar Jurado");
                    
                    System.out.println("0. Sair");

                    opcaoInterna = Utils.readLineFromConsole("Introduza opção: ");
                    
                    if(opcaoInterna.equals("1")){
                        ui.run();
                    }
                    
                    if(opcaoInterna.equals("2")){

                        ui.run_del();
                    }
                    
                    if(opcaoInterna.equals("3")){
                        ui.run_consult_esp();
                        ui.run_edit();
                    }
                    
                }while(!opcaoInterna.equals("0"));
                
            }
            
            if(opcao.equals("6"))
            {
                do{
                    EditarJuriUI ui = new EditarJuriUI(conn);
                    System.out.println("\n*****DADOS DE JURI*****");
                    ui.run_consult();
                    System.out.println("***********************\n");
                    System.out.println("1. Adicionar Juri");
                    System.out.println("2. Eliminar Juri");
                    System.out.println("3. Editar Juri");
                    
                    System.out.println("0. Sair");

                    opcaoInterna = Utils.readLineFromConsole("Introduza opção: ");
                    
                    if(opcaoInterna.equals("1")){

                        ui.run();
                    }
                    
                    if(opcaoInterna.equals("2")){
                       
                        EditarJuradoUI ui2 = new EditarJuradoUI(conn);
                        EditarClassificacaoUI ui3 = new EditarClassificacaoUI(conn);
                        System.out.println("\n*****DADOS DE JURI*****");
                        ui.run_consult();
                        System.out.println("*************************\n");
                        System.out.println("\n*****DADOS DE JURADO*****");
                        ui2.run_consult();
                        System.out.println("*****************************\n");
                        System.out.println("\n***DADOS DE CLASSIFICACAO**");
                        ui3.run_consult();
                        System.out.println("*****************************\n");
                        
                        System.out.println("******Retire os jurados e classificacões atribuidas pelo juri que quer eliminar********");
                        System.out.println("O Juri atribuiu classificacao a uma prova? ");
                        String TerClassificacao = Utils.readLineFromConsole("Sim/Nao");
                        if(TerClassificacao.equals("Sim")){
                            ui.run_del();
                        }
                        
                        String continuar="";
                        System.out.println("O Juri tem jurados? ");
                        String TerJurados = Utils.readLineFromConsole("Sim/Nao");
                        
                        if(TerJurados.equals("Sim")){
                            do{
                                ui2.run_del();
                                System.out.println("Eliminar mais jurados?");
                                continuar = Utils.readLineFromConsole("Sim/Nao");
                            }while(!continuar.equals("Nao"));
                        }
                        
                        
                        ui.run_del();
                    }
                    
                    if(opcaoInterna.equals("3")){
                        
                        ui.run_consult_esp();
                        ui.run_edit();
                    }
                    
                }while(!opcaoInterna.equals("0"));
                
            }
           
            
            if(opcao.equals("7"))
            {
                EditarRobotUI ui = new EditarRobotUI(conn);
                System.out.println("\n*********DADOS DO ROBOT*********");
                ui.run_consult();
                System.out.println("**********************************\n");
            }
            
            if(opcao.equals("9"))
            {
                EditarProvaUI ui = new EditarProvaUI(conn);
                System.out.println("\n*********DADOS DA PROVA*********");
                ui.run_consult();
                System.out.println("**********************************\n");
            }
            
            if(opcao.equals("10"))
            {
                System.out.println("\n*********DADOS DA CLASSIFICAÇÃO*********");
                EditarClassificacaoUI ui = new EditarClassificacaoUI(conn);
                ui.run_consult_esp();
                System.out.println("******************************************\n");
            }
            
            if(opcao.equals("11")){
                EditarElementoUI ui = new EditarElementoUI(conn);
                ui.run_consult_mudancas();
            }
            
            if(opcao.equals("12")){
                CirclesAnimation ca = new CirclesAnimation();
                JFrame jf = new JFrame();
                jf.setTitle("Circles");
                jf.setSize(1000,400);
                jf.setVisible(true);
                jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                jf.add(ca);
            }
        }
        while (!opcao.equals("0") );
    }
}

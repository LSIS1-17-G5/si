/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import com.mysql.jdbc.Connection;
import controller.EditarEquipaController;
import utils.Utils;

/**
 *
 * @author Antonio
 */
public class EditarEquipaUI {
    private final EditarEquipaController controller;
    
    public EditarEquipaUI(Connection connector)
    {
        controller = new EditarEquipaController(connector);
    }
    
    public void IntroduzirDados()
    {
        String ID=Utils.readLineFromConsole("Numero da equipa:");
        String Nome=Utils.readLineFromConsole("Nome da equipa:");
        
        controller.AdicionarEquipa(ID, Nome);
    }
    
    public void EliminarDados(){
        String ID=Utils.readLineFromConsole("Numero da equipa:");
        
        controller.EliminarEquipa(ID);
    }
    
    public void EditarDados(){
        System.out.println("Que equipa pretende editar?(introduza numero de equipa)");
        String ID_Original=Utils.readLineFromConsole("Numero de Equipa Original:");
        System.out.println("Introduza dados:");
        String ID=Utils.readLineFromConsole("Numero da Equipa:");
        String Nome=Utils.readLineFromConsole("Nome da equipa");
        
        controller.EditarEquipa(ID, Nome, ID_Original);
    }
    
    public void ConsultarDados()
    {
        controller.ConsultarDadosEquipa();
    }
    
    public void ConsultarDados_Esp()
    {
        String ID_Equipa = Utils.readLineFromConsole("Introduza o número da equipa: ");
        controller.ConsultarDadosEquipa_Esp(ID_Equipa);
    }
    
    public void run(){
        IntroduzirDados();
    }
    public void run_del(){
        EliminarDados();
    }
    public void run_edit(){
        EditarDados();
    }
    public void run_consult(){
        ConsultarDados();
    }
    public void run_consult_esp(){
        ConsultarDados_Esp();
    }
}

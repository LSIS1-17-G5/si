/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import com.mysql.jdbc.Connection;
import controller.EditarJuriController;
import utils.Utils;

/**
 *
 * @author Antonio
 */
public class EditarJuriUI {
    private final EditarJuriController controller;
    
    public EditarJuriUI(Connection connector)
    {
        controller = new EditarJuriController(connector);
    }
    
    public void IntroduzirDados()
    {
        String CodigoJuri=Utils.readLineFromConsole("ID do Juri:");
        String HoraJuri=Utils.readLineFromConsole("Hora de presença do Juri:");
        
        controller.AdicionarJuri(CodigoJuri, HoraJuri);
    }
    
    public void EliminarDados(){
        String CodigoJuri=Utils.readLineFromConsole("ID do juri a eliminar:");
        
        controller.EliminarJuri(CodigoJuri);
    }
    
    public void EditarDados(){
        System.out.println("Que juri pretende editar?(introduza ID)");
        String CodigoJuri_Original=Utils.readLineFromConsole("ID do Juri Original:");
        System.out.println("Introduza dados:");
        String CodigoJuri=Utils.readLineFromConsole("ID do Juri:");
        String HoraJuri=Utils.readLineFromConsole("Hora de presença do Juri:");
        
        controller.EditarJuri(CodigoJuri, HoraJuri, CodigoJuri_Original);
    }
    
    public void ConsultarDados()
    {
        controller.ConsultarDadosJuri();
    }
    
    public void ConsultarDados_Esp()
    {
        String ID_Juri = Utils.readLineFromConsole("Introduza o id do júri: ");
        controller.ConsultarDadosJuri_Esp(ID_Juri);
    }
    
    public void run(){
        IntroduzirDados();
    }
    public void run_del(){
        EliminarDados();
    }
    public void run_edit(){
        EditarDados();
    }
    
    public void run_consult_esp(){
        ConsultarDados_Esp();
    }
    
    public void run_consult(){
        ConsultarDados();
    }

}

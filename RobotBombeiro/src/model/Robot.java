/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Antonio
 */
public class Robot {
    private String m_ID;
    private String m_ModoLigacao;
    private String m_ModoApagar;
    private double m_Comp;
    private double m_Larg;
    private double m_Alt;
    private int m_NumSensores;
    private String m_TipoSonar;
    private String m_TipoChama;
    private double m_Tensao;
    private double m_Corrente;
    private String ID_Equipa;

    
    public Robot(){}
    
    public Robot(String m_ID, String m_ModoLigacao, String m_ModoApagar, double m_Comp, double m_Larg, double m_Alt, int m_NumSensores, 
            String m_TipoSonar, String m_TipoChama, double m_Tensao, double m_Corrente, String ID_Equipa) {
        this.m_ID=m_ID;
        this.m_ModoLigacao=m_ModoLigacao;
        this.m_ModoApagar=m_ModoApagar;
        this.m_Comp=m_Comp;
        this.m_Larg=m_Larg;
        this.m_Alt=m_Alt;
        this.m_NumSensores=m_NumSensores;
        this.m_TipoSonar=m_TipoSonar;
        this.m_TipoChama=m_TipoChama;
        this.m_Tensao=m_Tensao;
        this.m_Corrente=m_Corrente;
        this.ID_Equipa=ID_Equipa;
    }

    /**
     * @return the m_ID
     */
    public String getM_ID() {
        return m_ID;
    }

    /**
     * @param m_ID the m_ID to set
     */
    public void setM_ID(String m_ID) {
        this.m_ID = m_ID;
    }

    /**
     * @return the m_ModoLigacao
     */
    public String getM_ModoLigacao() {
        return m_ModoLigacao;
    }

    /**
     * @param m_ModoLigacao the m_ModoLigacao to set
     */
    public void setM_ModoLigacao(String m_ModoLigacao) {
        this.m_ModoLigacao = m_ModoLigacao;
    }

    /**
     * @return the m_ModoApagar
     */
    public String getM_ModoApagar() {
        return m_ModoApagar;
    }

    /**
     * @param m_ModoApagar the m_ModoApagar to set
     */
    public void setM_ModoApagar(String m_ModoApagar) {
        this.m_ModoApagar = m_ModoApagar;
    }

    /**
     * @return the m_Comp
     */
    public double getM_Comp() {
        return m_Comp;
    }

    /**
     * @param m_Comp the m_Comp to set
     */
    public void setM_Comp(double m_Comp) {
        this.m_Comp = m_Comp;
    }

    /**
     * @return the m_Larg
     */
    public double getM_Larg() {
        return m_Larg;
    }

    /**
     * @param m_Larg the m_Larg to set
     */
    public void setM_Larg(double m_Larg) {
        this.m_Larg = m_Larg;
    }

    /**
     * @return the m_Alt
     */
    public double getM_Alt() {
        return m_Alt;
    }

    /**
     * @param m_Alt the m_Alt to set
     */
    public void setM_Alt(double m_Alt) {
        this.m_Alt = m_Alt;
    }

    /**
     * @return the m_NumSensores
     */
    public int getM_NumSensores() {
        return m_NumSensores;
    }

    /**
     * @param m_NumSensores the m_NumSensores to set
     */
    public void setM_NumSensores(int m_NumSensores) {
        this.m_NumSensores = m_NumSensores;
    }

    /**
     * @return the m_TipoSonar
     */
    public String getM_TipoSonar() {
        return m_TipoSonar;
    }

    /**
     * @param m_TipoSonar the m_TipoSonar to set
     */
    public void setM_TipoSonar(String m_TipoSonar) {
        this.m_TipoSonar = m_TipoSonar;
    }

    /**
     * @return the m_TipoChama
     */
    public String getM_TipoChama() {
        return m_TipoChama;
    }

    /**
     * @param m_TipoChama the m_TipoChama to set
     */
    public void setM_TipoChama(String m_TipoChama) {
        this.m_TipoChama = m_TipoChama;
    }

    /**
     * @return the m_Tensao
     */
    public double getM_Tensao() {
        return m_Tensao;
    }

    /**
     * @param m_Tensao the m_Tensao to set
     */
    public void setM_Tensao(double m_Tensao) {
        this.m_Tensao = m_Tensao;
    }

    /**
     * @return the m_Corrente
     */
    public double getM_Corrente() {
        return m_Corrente;
    }

    /**
     * @param m_Corrente the m_Corrente to set
     */
    public void setM_Corrente(double m_Corrente) {
        this.m_Corrente = m_Corrente;
    }

    /**
     * @return the ID_Equipa
     */
    public String getID_Equipa() {
        return ID_Equipa;
    }

    /**
     * @param ID_Equipa the ID_Equipa to set
     */
    public void setID_Equipa(String ID_Equipa) {
        this.ID_Equipa = ID_Equipa;
    }
    
    
        
}

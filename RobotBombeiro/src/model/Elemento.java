/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Antonio
 */
public class Elemento {
    private int m_ID;
    private String m_Nome;
    private int m_Idade;
    private int m_NumeroElem;
    private String m_NomeEscola;
    private String m_ID_Equipa;

    
    public Elemento(){}
    
    public Elemento(int m_ID, String m_Nome, int m_Idade, int m_NumeroElem, String m_NomeEscola,String m_ID_Equipa){
        this.m_ID=m_ID;
        this.m_Idade=m_Idade;
        this.m_Nome=m_Nome;
        this.m_NomeEscola=m_NomeEscola;
        this.m_NumeroElem=m_NumeroElem;
        this.m_ID_Equipa=m_ID_Equipa;
    }
    
    /**
     * @return the m_ID
     */
    public int getM_ID() {
        return m_ID;
    }

    /**
     * @param m_ID the m_ID to set
     */
    public void setM_ID(int m_ID) {
        this.m_ID = m_ID;
    }

    /**
     * @return the m_Nome
     */
    public String getM_Nome() {
        return m_Nome;
    }

    /**
     * @param m_Nome the m_Nome to set
     */
    public void setM_Nome(String m_Nome) {
        this.m_Nome = m_Nome;
    }

    /**
     * @return the m_Idade
     */
    public int getM_Idade() {
        return m_Idade;
    }

    /**
     * @param m_Idade the m_Idade to set
     */
    public void setM_Idade(int m_Idade) {
        this.m_Idade = m_Idade;
    }

    /**
     * @return the m_NumeroElem
     */
    public int getM_NumeroElem() {
        return m_NumeroElem;
    }

    /**
     * @param m_NumeroElem the m_NumeroElem to set
     */
    public void setM_NumeroElem(int m_NumeroElem) {
        this.m_NumeroElem = m_NumeroElem;
    }

    /**
     * @return the m_NomeEscola
     */
    public String getM_NomeEscola() {
        return m_NomeEscola;
    }

    /**
     * @param m_NomeEscola the m_NomeEscola to set
     */
    public void setM_NomeEscola(String m_NomeEscola) {
        this.m_NomeEscola = m_NomeEscola;
    }
    
    /**
     * @return the m_ID_Equipa
     */
    public String getM_ID_Equipa() {
        return m_ID_Equipa;
    }

    /**
     * @param m_ID the m_ID_Equipa to set
     */
    public void setM_ID_Equipa(String m_ID_Equipa) {
        this.m_ID_Equipa = m_ID_Equipa;
    }
    
    @Override
    public String toString()
    {
        return this.m_ID + ";" + this.m_Nome + ";" + this.m_Idade + ";" + this.m_NumeroElem + ";" + this.m_NomeEscola;
    }
    
}

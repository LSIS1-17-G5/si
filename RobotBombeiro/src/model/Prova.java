/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Antonio
 */
public class Prova {
   private String m_Codigo;
   private String m_DiaHora;
   //private int m_Classificacao;
   private String m_ID_Equipa;
   
   public Prova(){}
   
   public Prova(String m_Codigo, String m_DiaHora, /*int m_Classificacao,*/ String m_ID_Equipa){
       //this.m_Classificacao=m_Classificacao;
       this.m_Codigo=m_Codigo;
       this.m_DiaHora=m_DiaHora;
       this.m_ID_Equipa=m_ID_Equipa;
   }

    /**
     * @return the m_Codigo
     */
    public String getM_Codigo() {
        return m_Codigo;
    }

    /**
     * @param m_Codigo the m_Codigo to set
     */
    public void setM_Codigo(String m_Codigo) {
        this.m_Codigo = m_Codigo;
    }

    /**
     * @return the m_DiaHora
     */
    public String getM_DiaHora() {
        return m_DiaHora;
    }

    /**
     * @param m_DiaHora the m_DiaHora to set
     */
    public void setM_DiaHora(String m_DiaHora) {
        this.m_DiaHora = m_DiaHora;
    }
    
    /**
     * @return the m_Classificacao
     */
    /*
    public int getM_Classificacao() {
        return m_Classificacao;
    }

    /**
     * @param m_Classificacao the m_Classificacao to set
     */
    /*
    public void setM_Classificacao(int m_Classificacao) {
        this.m_Classificacao = m_Classificacao;
    }*/

    /**
     * @return the m_ID_Equipa
     */
    public String getM_ID_Equipa() {
        return m_ID_Equipa;
    }

    /**
     * @param m_ID_Equipa the m_ID_Equipa to set
     */
    public void setM_ID_Equipa(String m_ID_Equipa) {
        this.m_ID_Equipa = m_ID_Equipa;
    }
   
   
}

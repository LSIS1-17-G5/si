/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Antonio
 */
public class Classificacao {
    private String m_ID_Classificacao;
    private int m_Class_Content;
    private int m_Class_Visual;
    private int m_Class_Presentation;
    private int m_Class_Total;
    private String m_ID_Prova;
    private String m_ID_Juri;
    
    public Classificacao(){}
    
    public Classificacao(String m_ID_Classificacao, int m_Class_Content, int m_Class_Visual, int m_Class_Presentation, int m_Class_Total, String m_ID_Prova, String m_ID_Juri){
        this.m_ID_Classificacao=m_ID_Classificacao;
        this.m_Class_Content=m_Class_Content;
        this.m_Class_Visual=m_Class_Visual;
        this.m_Class_Presentation=m_Class_Presentation;
        this.m_Class_Total=m_Class_Total;
        this.m_ID_Prova=m_ID_Prova;
        this.m_ID_Juri=m_ID_Juri;
    }

    /**
     * @return the m_ID_Classificacao
     */
    public String getM_ID_Classificacao() {
        return m_ID_Classificacao;
    }

    /**
     * @param m_ID_Classificacao the m_ID_Classificacao to set
     */
    public void setM_ID_Classificacao(String m_ID_Classificacao) {
        this.m_ID_Classificacao = m_ID_Classificacao;
    }

    /**
     * @return the m_Class_Content
     */
    public int getM_Class_Content() {
        return m_Class_Content;
    }

    /**
     * @param m_Class_Content the m_Class_Content to set
     */
    public void setM_Class_Content(int m_Class_Content) {
        this.m_Class_Content = m_Class_Content;
    }

    /**
     * @return the m_Class_Visual
     */
    public int getM_Class_Visual() {
        return m_Class_Visual;
    }

    /**
     * @param m_Class_Visual the m_Class_Visual to set
     */
    public void setM_Class_Visual(int m_Class_Visual) {
        this.m_Class_Visual = m_Class_Visual;
    }

    /**
     * @return the m_Class_Presentation
     */
    public double getM_Class_Presentation() {
        return m_Class_Presentation;
    }

    /**
     * @param m_Class_Presentation the m_Class_Presentation to set
     */
    public void setM_Class_Presentation(int m_Class_Presentation) {
        this.m_Class_Presentation = m_Class_Presentation;
    }

    /**
     * @return the m_Class_Total
     */
    public int getM_Class_Total() {
        return m_Class_Total;
    }

    /**
     * @param m_Class_Total the m_Class_Total to set
     */
    public void setM_Class_Total(int m_Class_Total) {
        this.m_Class_Total = m_Class_Total;
    }

    /**
     * @return the m_ID_Prova
     */
    public String getM_ID_Prova() {
        return m_ID_Prova;
    }

    /**
     * @param m_ID_Prova the m_ID_Prova to set
     */
    public void setM_ID_Prova(String m_ID_Prova) {
        this.m_ID_Prova = m_ID_Prova;
    }

    /**
     * @return the m_ID_Juri
     */
    public String getM_ID_Juri() {
        return m_ID_Juri;
    }

    /**
     * @param m_ID_Juri the m_ID_Juri to set
     */
    public void setM_ID_Juri(String m_ID_Juri) {
        this.m_ID_Juri = m_ID_Juri;
    }
    
    
}

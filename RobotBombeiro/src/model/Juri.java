/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Antonio
 */
public class Juri {
    private String m_CodigoJuri;
    private String m_HoraJuri;
    
    public Juri(){}
    
    public Juri(String m_CodigoJuri, String m_HoraJuri){
        this.m_CodigoJuri=m_CodigoJuri;
        this.m_HoraJuri=m_HoraJuri;
    }

    /**
     * @return the m_CodigoJuri
     */
    public String getM_CodigoJuri() {
        return m_CodigoJuri;
    }

    /**
     * @param m_CodigoJuri the m_CodigoJuri to set
     */
    public void setM_CodigoJuri(String m_CodigoJuri) {
        this.m_CodigoJuri = m_CodigoJuri;
    }

    /**
     * @return the m_HoraJuri
     */
    public String getM_HoraJuri() {
        return m_HoraJuri;
    }

    /**
     * @param m_HoraJuri the m_HoraJuri to set
     */
    public void setM_HoraJuri(String m_HoraJuri) {
        this.m_HoraJuri = m_HoraJuri;
    }
    
    
}

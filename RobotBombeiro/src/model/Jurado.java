/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Antonio
 */
public class Jurado {
    private String m_CodigoJurado;
    private String m_Nome;
    private String m_Email;
    private int m_Idade;
    private String m_ID_Juri;
    
    public Jurado(){}
    
    public Jurado(String m_CodigoJurado, String m_Nome, String m_Email, int m_Idade, String m_ID_Juri){
        this.m_CodigoJurado=m_CodigoJurado;
        this.m_Nome=m_Nome;
        this.m_Email=m_Email;
        this.m_Idade=m_Idade;
        this.m_ID_Juri=m_ID_Juri;
    }
    /**
     * @return the m_CodigoJurado
     */
    public String getM_CodigoJurado() {
        return m_CodigoJurado;
    }

    /**
     * @param m_CodigoJurado the m_CodigoJurado to set
     */
    public void setM_CodigoJurado(String m_CodigoJurado) {
        this.m_CodigoJurado = m_CodigoJurado;
    }

    /**
     * @return the m_Nome
     */
    public String getM_Nome() {
        return m_Nome;
    }

    /**
     * @param m_Nome the m_Nome to set
     */
    public void setM_Nome(String m_Nome) {
        this.m_Nome = m_Nome;
    }

    /**
     * @return the m_Email
     */
    public String getM_Email() {
        return m_Email;
    }

    /**
     * @param m_Email the m_Email to set
     */
    public void setM_Email(String m_Email) {
        this.m_Email = m_Email;
    }

    /**
     * @return the m_Idade
     */
    public int getM_Idade() {
        return m_Idade;
    }

    /**
     * @param m_Idade the m_Idade to set
     */
    public void setM_Idade(int m_Idade) {
        this.m_Idade = m_Idade;
    }

    /**
     * @return the m_ID_Juri
     */
    public String getM_ID_Juri() {
        return m_ID_Juri;
    }

    /**
     * @param m_ID_Juri the m_ID_Juri to set
     */
    public void setM_ID_Juri(String m_ID_Juri) {
        this.m_ID_Juri = m_ID_Juri;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Antonio
 */

public class Equipa {
    private String m_ID;
    private String m_Nome;
    private final List<Elemento> m_lstElemento;
    private final List<Prova> m_lstProva;
    private final List<Robot> m_lstRobot;
    /*private String m_Escalao; Comentei isto porque não sei se o uso como atributo logo quando se cria a equipa ou depois de ter os membros todos*/
    
    
    public Equipa(){
        this.m_lstElemento=new ArrayList();
        this.m_lstProva=new ArrayList();
        this.m_lstRobot=new ArrayList();
    }
    public Equipa(String m_ID, String m_Nome){
        this.m_ID=m_ID;
        this.m_Nome=m_Nome;
        this.m_lstElemento=new ArrayList();
        this.m_lstProva=new ArrayList();
        this.m_lstRobot=new ArrayList();
    }
    /**
     * @return the m_ID
     */
    public String  getM_ID() {
        return m_ID;
    }

    /**
     * @param m_ID the m_ID to set
     */
    public void setM_ID(String m_ID) {
        this.m_ID = m_ID;
    }

    /**
     * @return the m_Nome
     */
    public String getM_Nome() {
        return m_Nome;
    }

    /**
     * @param m_Nome the m_Nome to set
     */
    public void setM_Nome(String m_Nome) {
        this.m_Nome = m_Nome;
    }
    
    @Override
    public String toString()
    {
        return this.m_ID + ";" + this.m_Nome;
    }
}

/*
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Jurado;

/**
 *
 * @author Antonio
 */
public class EditarJuradoController {

    Connection conn;
    
    public EditarJuradoController(Connection connection){
        
        conn = connection;
    }
    
    //Boolean para indicar se a operação foi bem sucedidda
    public boolean AdicionarJurado(String CodigoJurado, String Nome, String Email, int Idade, String ID_Juri){
        try{
            Jurado jurado = new Jurado(CodigoJurado, Nome, Email, Idade, ID_Juri);
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("INSERT INTO jurado (CodigoJurado, Nome, Email, Idade, ID_Juri) "
                    + "VALUES ('" + jurado.getM_CodigoJurado() + "','" + jurado.getM_Nome() + "','" + jurado.getM_Email() + "','" 
                    + jurado.getM_Idade() + "','" + jurado.getM_ID_Juri() + "')");
            //statement.executeUpdate("INSERT INTO elemento (NIF, Nome, Idade, Numero, NomeEscola) VALUES ('2','b','2','2','b')");
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public boolean EditarJurado(String CodigoJurado, String Nome, String Email, int Idade, String ID_Juri, String CodigoJurado_Original){
        try{
            Jurado jurado = new Jurado(CodigoJurado, Nome, Email, Idade, ID_Juri);
            
            String query = "UPDATE jurado SET CodigoJurado=?, Nome=?, Email=?, Idade=?, ID_Juri=? WHERE CodigoJurado=?";
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setString(1, CodigoJurado);
            preparedStmt.setString(2, Nome);
            preparedStmt.setString(3, Email);
            preparedStmt.setInt(4, Idade);
            preparedStmt.setString(5, ID_Juri);
            preparedStmt.setString(6, CodigoJurado_Original);
            
            preparedStmt.execute();
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public boolean EliminarJurado(String CodigoJurado){
        try{
            
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("DELETE FROM jurado WHERE CodigoJurado=" + "'"+ CodigoJurado +"'");
            //statement.executeUpdate("INSERT INTO elemento (NIF, Nome, Idade, Numero, NomeEscola) VALUES ('2','b','2','2','b')");
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public void ConsultarDadosJurado_Esp(String CodigoJurado1){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM jurado WHERE CodigoJurado= '" +CodigoJurado1+ "'";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String CodigoJurado = rs.getString("CodigoJurado");
            String Nome = rs.getString("Nome");
            String Email = rs.getString("Email");
            int Idade = rs.getInt("Idade");
            String ID_Juri = rs.getString("ID_Juri");
           
        
            // print the results
            
            System.out.format("ID Jurado: %s\nNome: %s\nE-mail: %s\nIdade: %s\nID Juri: %s\n", CodigoJurado, Nome, Email, 
                    Idade, ID_Juri);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
    
    public void ConsultarDadosJurado(){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM jurado";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String CodigoJurado = rs.getString("CodigoJurado");
            String Nome = rs.getString("Nome");
            String Email = rs.getString("Email");
            int Idade = rs.getInt("Idade");
            String ID_Juri = rs.getString("ID_Juri");
           
        
            // print the results
            
            System.out.format("ID Jurado: %s\nNome: %s\nE-mail: %s\nIdade: %s\nID Juri: %s\n", CodigoJurado, Nome, Email, 
                    Idade, ID_Juri);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
}
}
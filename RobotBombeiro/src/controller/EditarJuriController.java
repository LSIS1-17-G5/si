/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.mysql.jdbc.Connection;
import model.Juri;

/**
 *
 * @author Antonio
 */
public class EditarJuriController {
    Connection conn;
    
    public EditarJuriController(Connection connection){
        conn = connection;
    }
    
    public boolean AdicionarJuri(String CodigoJuri, String HoraJuri){
        try{
        Juri juri = new Juri(CodigoJuri, HoraJuri);
        Statement statement = (Statement) conn.createStatement();
        statement.executeUpdate("INSERT INTO juri (CodigoJuri, HoraJuri) "
                    + "VALUES ('" + juri.getM_CodigoJuri() + "','" + juri.getM_HoraJuri() +"')");
        
        }catch(Exception e){
             System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean EditarJuri(String CodigoJuri, String HoraJuri, String CodigoJuri_Original){
        
        try{
            
            Juri juri = new Juri(CodigoJuri, HoraJuri);
            
            String query = "UPDATE juri SET CodigoJuri=?, HoraJuri=? WHERE CodigoJuri=?";
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setString(1, CodigoJuri);
            preparedStmt.setString(2, HoraJuri);
            preparedStmt.setString(3, CodigoJuri_Original);
            
            preparedStmt.execute();
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public boolean EliminarJuri(String CodigoJuri){
        try{
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("DELETE FROM juri WHERE CodigoJuri=" + "'"+CodigoJuri+"'");
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public void ConsultarDadosJuri_Esp(String ID_Juri1){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM juri WHERE CodigoJuri= '" +ID_Juri1+ "'";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String CodigoJuri = rs.getString("CodigoJuri");
            String HoraJuri = rs.getString("HoraJuri");
        
            // print the results
            
            System.out.format("Codigo Juri: %s\nHora de presença:%s\n", CodigoJuri, HoraJuri);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
    
    public void ConsultarDadosJuri(){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM juri";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String CodigoJuri = rs.getString("CodigoJuri");
            String HoraJuri = rs.getString("HoraJuri");
        
            // print the results
            
            System.out.format("Codigo Juri: %s\nHora de presença:%s\n", CodigoJuri, HoraJuri);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
}
}

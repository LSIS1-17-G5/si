/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Elemento;

/**
 *
 * @author Antonio
 */
public class EditarElementoController {

    List<Elemento> elementos;
    Connection conn;
    
    public EditarElementoController(Connection connection){
        
        elementos = new ArrayList<>();
        conn = connection;
    }
    
    //Boolean para indicar se a operação foi bem sucedidda
    public boolean AdicionarElemento(String nome, int idade, int numero, String escola, int NIF, String equipa){
        try{
            Elemento elemento = new Elemento(NIF, nome, idade, numero, escola, equipa);
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("INSERT INTO elemento (NIF, Nome, Idade, Numero, NomeEscola, ID_Equipa) "
                    + "VALUES ('" + elemento.getM_ID() + "','" + elemento.getM_Nome() + "','" + elemento.getM_Idade() + "','" 
                    + elemento.getM_NumeroElem() + "','" + elemento.getM_NomeEscola() + "','" + elemento.getM_ID_Equipa()+ "')");
            //statement.executeUpdate("INSERT INTO elemento (NIF, Nome, Idade, Numero, NomeEscola) VALUES ('2','b','2','2','b')");
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public boolean EditarElemento(int NIF, String nome, int idade, int numero, String escola, int NIF_Original, String equipa, String equipa_original){
        try{
            Elemento elemento = new Elemento(NIF, nome, idade, numero, escola, equipa);
            /*Statement statement = (Statement) conn.createStatement();*/
            /*statement.executeUpdate("UPDATE elemento SET 'NIF'="+elemento.getM_ID()+",'Nome'="+elemento.getM_Nome()+
                    ",'Idade'="+elemento.getM_Idade()+",'Numero'="+elemento.getM_NumeroElem()
                    +",'NomeEscola'="+elemento.getM_NomeEscola()+" WHERE 'NIF'="+NIF_Original);*/
            /*statement.executeUpdate("UPDATE `elemento` SET `NIF`=" + elemento.getM_ID() + ",`Nome`=" + elemento.getM_Nome() + ",`Idade`=" + elemento.getM_Idade() + ",`Numero`=" + elemento.getM_NumeroElem() + ",`NomeEscola`=" + elemento.getM_NomeEscola() + " WHERE 'NIF'=" + NIF_Original);*/
            /*statement.executeUpdate("UPDATE `elemento` SET `NIF`=" +"\""+ elemento.getM_ID()+"\"" + ",`Nome`=" +"\""+ elemento.getM_Nome()+"\""+ ",`Idade`=" +"\""+ elemento.getM_Idade() +"\""+ ",`Numero`=" +"\""+elemento.getM_NumeroElem() +"\""+ ",`NomeEscola`=" +"\""+ elemento.getM_NomeEscola() +"\""+ " WHERE 'NIF'=" +"\""+NIF_Original+"\"");*/
            String query = "UPDATE elemento SET NIF=?, Nome=?, Idade=?, Numero=?, NomeEscola=?, ID_Equipa=? WHERE NIF=?";
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("INSERT INTO mudanca (NIF, Nome, ID_Equipa_Original, ID_Equipa) "+ "VALUES ('"+elemento.getM_ID() + "','"+ elemento.getM_Nome() + "','"+ equipa_original +"','"+ elemento.getM_ID_Equipa()+"')");
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setInt(1, NIF);
            preparedStmt.setString(2, nome);
            preparedStmt.setInt(3, idade);
            preparedStmt.setInt(4, numero);
            preparedStmt.setString(5, escola);
            preparedStmt.setString(6, equipa);
            preparedStmt.setInt(7, NIF_Original);
            
            preparedStmt.execute();
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public boolean EliminarElemento(int NIF){
        try{
            
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("DELETE FROM elemento WHERE NIF=" + NIF);
            //statement.executeUpdate("INSERT INTO elemento (NIF, Nome, Idade, Numero, NomeEscola) VALUES ('2','b','2','2','b')");
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public void ConsultarDadosElemento_Esp(String NIF1){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM elemento WHERE NIF= '" +NIF1+ "'";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String NIF = rs.getString("NIF");
            String Nome = rs.getString("Nome");
            int Idade = rs.getInt("Idade");
            int Numero = rs.getInt("Numero");
            String NomeEscola= rs.getString("NomeEscola");
            String ID_Equipa = rs.getString("ID_Equipa");
           
        
            // print the results
            
            System.out.format("NIF: %s\nNome: %s\nIdade: %s\nNumero: %s\nNome da Escola: %s\nID_Equipa %s\n", NIF, Nome, Idade, 
                    Numero, NomeEscola, ID_Equipa);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
    
    public void ConsultarDadosElemento(){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM elemento";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String NIF = rs.getString("NIF");
            String Nome = rs.getString("Nome");
            int Idade = rs.getInt("Idade");
            int Numero = rs.getInt("Numero");
            String NomeEscola= rs.getString("NomeEscola");
            String ID_Equipa = rs.getString("ID_Equipa");
           
        
            // print the results
            
            System.out.format("NIF: %s\nNome: %s\nIdade: %s\nNumero: %s\nNome da Escola: %s\nID_Equipa %s\n", NIF, Nome, Idade, 
                    Numero, NomeEscola, ID_Equipa);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
    
    public void ConsultarMudancas(){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM mudanca";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String NIF = rs.getString("NIF");
            String Nome = rs.getString("Nome");
            String ID_Equipa_Original = rs.getString("ID_Equipa_Original");
            String ID_Equipa = rs.getString("ID_Equipa");
           
        
            // print the results
            
            System.out.format("NIF: %s\nNome: %s\nID_Equipa_Original: %s\nID_Equipa: %s\n", NIF, Nome, ID_Equipa_Original, ID_Equipa);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }

}

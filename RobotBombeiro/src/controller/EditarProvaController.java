/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.util.Date;
import model.Prova;

/**
 *
 * @author Antonio
 */
public class EditarProvaController {
    Connection conn;
    
    public EditarProvaController(Connection connection){
        conn = connection;
    }
    
    public boolean AdicionarProva(String codigo, String DiaHora, /*int Classificacao,*/ String ID_Equipa){
        try
        {
            //REMINDER: CLASSIFICACAO ESTA COMENTADO PARA TESTE
            Prova prova = new Prova(codigo, DiaHora,/*Classificacao,*/ ID_Equipa);
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("INSERT INTO prova (Codigo, DiaHora, ID_Equipa) "
                    + "VALUES ('" + prova.getM_Codigo() + "','" + prova.getM_DiaHora() + "','" + prova.getM_ID_Equipa() + "')");
        }catch(Exception e)
        {
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        
        }
        return true;
    }
    
    public boolean EditarProva(String codigo, String DiaHora, /*int Classificacao,*/ String codigo_Original, String ID_Equipa)
    {
    try
        {
            Prova prova = new Prova(codigo, DiaHora,/*Classificacao,*/ ID_Equipa);
            String query = "UPDATE prova SET Codigo=?, DiaHora=?, ID_Equipa=? WHERE Codigo=?";
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setString(1, codigo);
            preparedStmt.setString(2, DiaHora);
            //preparedStmt.setInt(3, Classificacao);
            preparedStmt.setString(3, ID_Equipa);
            preparedStmt.setString(4, codigo_Original);
            
            preparedStmt.execute();
        }catch(Exception e)
        {
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        
        }
        return true;
    }
    
    
    public boolean EliminarProva(String codigo)
    {
        try
        {
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("DELETE FROM prova WHERE Codigo=" + "'"+codigo+"'");
        
        }catch(Exception e)
        {
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        
        }
        return true;
    }
    
    public void ConsultarDadosProva(String Codigo1){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM prova WHERE Codigo= '" +Codigo1+ "'";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String Codigo = rs.getString("Codigo");
            String DiaHora = rs.getString("DiaHora");
            String ID_Equipa = rs.getString("ID_Equipa");
            
            // print the results
            
            System.out.format("Equipa:%s\nData e Hora: %s", ID_Equipa, DiaHora);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
    
    public void ConsultarDadosProva_Esp(){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM prova";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String Codigo = rs.getString("Codigo");
            String DiaHora = rs.getString("DiaHora");
            String ID_Equipa = rs.getString("ID_Equipa");
            
            // print the results
            
            System.out.format("Codigo: %s\nData e Hora: %s\nEquipa:%s\n", Codigo, DiaHora, ID_Equipa);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
}

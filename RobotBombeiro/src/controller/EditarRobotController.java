/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Robot;


/**
 *
 * @author Antonio
 */
public class EditarRobotController {
    Connection conn;
    
    public EditarRobotController(Connection connection){
        
        conn = connection;
    }
    
    public boolean AdicionarRobot(String ID, String ModoLigacao, String ModoApagar, double Comp, double Larg, double Alt, int NumSensores, String TipoSonar, String TipoChama, double Tensao, double Corrente, String ID_Equipa)
    {
        try{
            Robot robot = new Robot(ID, ModoLigacao, ModoApagar, Comp, Larg, Alt, NumSensores, TipoSonar, TipoChama, Tensao, Corrente, ID_Equipa);
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("INSERT INTO robot (ID_Robot, ModoLigacao, ModoApagar, Comprimento, Largura, Altura, NumSensores, TipoSonar, TipoChama, Tensao, Corrente, ID_Equipa) "
                    + "VALUES ('" + robot.getM_ID() + "','" + robot.getM_ModoLigacao() + "','" + robot.getM_ModoApagar() + "','" 
                    + robot.getM_Comp() + "','" + robot.getM_Larg() + "','" + robot.getM_Alt()+ "','" + robot.getM_NumSensores()+ "','"+ robot.getM_TipoSonar()+"','"
                    + robot.getM_TipoChama()+"','"+robot.getM_Tensao()+"','"+robot.getM_Corrente()+ "','" + robot.getID_Equipa()+ "')");
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public boolean EditarRobot(String ID, String ModoLigacao, String ModoApagar, double Comp, double Larg, double Alt, int NumSensores, String TipoSonar, String TipoChama, double Tensao, double Corrente, String ID_Original, String ID_Equipa)
    {
    try
        {
            Robot robot = new Robot(ID, ModoLigacao, ModoApagar, Comp, Larg, Alt, NumSensores, TipoSonar, TipoChama, Tensao, Corrente, ID_Equipa);
            String query = "UPDATE robot SET ID_Robot=?, ModoLigacao=?, ModoApagar=?, Comprimento=?, Largura=?, Altura=?, NumSensores=?, TipoSonar=?, TipoChama=?, Tensao=?, Corrente=?, ID_Equipa=?WHERE ID_Robot=?";
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setString(1, ID);
            preparedStmt.setString(2, ModoLigacao);
            preparedStmt.setString(3, ModoApagar);
            preparedStmt.setDouble(4, Comp);
            preparedStmt.setDouble(5, Larg);
            preparedStmt.setDouble(6, Alt);
            preparedStmt.setInt(7, NumSensores);
            preparedStmt.setString(8, TipoSonar);
            preparedStmt.setString(9, TipoChama);
            preparedStmt.setDouble(10, Tensao);
            preparedStmt.setDouble(11, Corrente);
            preparedStmt.setString(12, ID_Equipa);
            preparedStmt.setString(13, ID_Original);
            
            
            preparedStmt.execute();
        }catch(Exception e)
        {
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        
        }
        return true;
    }
    
    public boolean EliminarRobot(String ID){
        try{      
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("DELETE FROM robot WHERE ID_Robot= " + "'"+ ID+ "'");
            //statement.executeUpdate("INSERT INTO elemento (NIF, Nome, Idade, Numero, NomeEscola) VALUES ('2','b','2','2','b')");
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public void ConsultarDadosRobot(String ID_Bot){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM robot WHERE ID_Robot= '" +ID_Bot+ "'";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String ID_Robot = rs.getString("ID_Robot");
            String ModoLigacao = rs.getString("ModoLigacao");
            String ModoApagar = rs.getString("ModoApagar");
            double Comprimento = rs.getDouble("Comprimento");
            double Largura = rs.getDouble("Largura");
            double Altura = rs.getDouble("Altura");
            int NumSensores = rs.getInt("NumSensores");
            String TipoSonar = rs.getString("TipoSonar");
            String TipoChama = rs.getString("TipoChama");
            double Tensao = rs.getDouble("Tensao");
            double Corrente = rs.getDouble("Corrente");
            String ID_Equipa = rs.getString("ID_Equipa");
        
            // print the results
            
            System.out.format("ID_Robot: %s\nModo de Ligação: %s\nModo de Extinção: %s\nComprimento: %s\nLargura: %s\nAltura: %s\nNumero de Sensores: %s\n"
                    + "Tipo de Sonar: %s\nTipo de sensor de Chama: %s\nTensao: %s\nCorrente: %s\nID_Equipa: %s\n", ID_Robot, ModoLigacao, ModoApagar, 
                    Comprimento, Largura, Altura, NumSensores, 
                    TipoSonar, TipoChama, Tensao, Corrente, ID_Equipa);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
    
    public void ConsultarDadosRobot_Esp(){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM robot";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String ID_Robot = rs.getString("ID_Robot");
            String ModoLigacao = rs.getString("ModoLigacao");
            String ModoApagar = rs.getString("ModoApagar");
            double Comprimento = rs.getDouble("Comprimento");
            double Largura = rs.getDouble("Largura");
            double Altura = rs.getDouble("Altura");
            int NumSensores = rs.getInt("NumSensores");
            String TipoSonar = rs.getString("TipoSonar");
            String TipoChama = rs.getString("TipoChama");
            double Tensao = rs.getDouble("Tensao");
            double Corrente = rs.getDouble("Corrente");
            String ID_Equipa = rs.getString("ID_Equipa");
        
            // print the results
            
            System.out.format("ID_Robot: %s\nModo de Ligação: %s\nModo de Extinção: %s\nComprimento: %s\nLargura: %s\nAltura: %s\nNumero de Sensores: %s\n"
                    + "Tipo de Sonar: %s\nTipo de sensor de Chama: %s\nTensao: %s\nCorrente: %s\nID_Equipa: %s\n", ID_Robot, ModoLigacao, ModoApagar, 
                    Comprimento, Largura, Altura, NumSensores, 
                    TipoSonar, TipoChama, Tensao, Corrente, ID_Equipa);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
}
    


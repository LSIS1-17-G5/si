/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import model.Classificacao;

/**
 *
 * @author Antonio
 */
public class EditarClassificacaoController {
    Connection conn;
    
    public EditarClassificacaoController(Connection connection){
        conn = connection;
    }
    
    public boolean AdicionarClassificacao(String ID_Classificacao, int Class_Content, int Class_Visual, int Class_Presentation, int Class_Total, String ID_Prova, String ID_Juri){
        try{
            Classificacao classificacao = new Classificacao(ID_Classificacao, Class_Content, Class_Visual, Class_Presentation, Class_Total, ID_Prova, ID_Juri);
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("INSERT INTO classificacao (ID_Classificacao, Class_Content, Class_Visual, Class_Presentation, Class_Total, ID_Prova, ID_Juri) "
                    + "VALUES ('" + classificacao.getM_ID_Classificacao() + "','" + classificacao.getM_Class_Content() + "','" + classificacao.getM_Class_Visual() + "','" 
                    + classificacao.getM_Class_Presentation() + "','" + classificacao.getM_Class_Total() + "','" + classificacao.getM_ID_Prova()+ "','"+ classificacao.getM_ID_Juri()+ "')");
            //statement.executeUpdate("INSERT INTO elemento (NIF, Nome, Idade, Numero, NomeEscola) VALUES ('2','b','2','2','b')");
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        
        return true;
    }
    
    public boolean EditarClassificacao(String ID_Classificacao, int Class_Content, int Class_Visual, int Class_Presentation, int Class_Total, String ID_Prova, String ID_Juri, String ID_Classificacao_Original){
        try{
            Classificacao classificacao = new Classificacao(ID_Classificacao, Class_Content, Class_Visual, Class_Presentation, Class_Total, ID_Prova, ID_Juri);
           
            String query = "UPDATE classificacao SET ID_Classificacao=?, Class_Content=?, Class_Visual=?, Class_Presentation=?, Class_Total=?, ID_Prova=? ID_Juri=? WHERE ID_Classificacao=?";
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setString(1, ID_Classificacao);
            preparedStmt.setInt(2, Class_Content);
            preparedStmt.setInt(3, Class_Visual);
            preparedStmt.setInt(4, Class_Presentation);
            preparedStmt.setInt(5, Class_Total);
            preparedStmt.setString(6, ID_Prova);
            preparedStmt.setString(7, ID_Juri);
            preparedStmt.setString(8, ID_Classificacao_Original);
            
            preparedStmt.execute();
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public boolean EliminarClassificacao(String ID_Classificacao)
    {
    try{
            
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("DELETE FROM classificacao WHERE ID_Classificacao=" + "'"+ID_Classificacao+"'");
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public void ConsultarDadosClassificacao_Esp(String ID_Prova1){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM classificacao WHERE ID_Prova= '" +ID_Prova1+ "'";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String ID_Classificacao = rs.getString("ID_Classificacao");
            int Class_Content = rs.getInt("Class_Content");
            int Class_Visual = rs.getInt("Class_Visual");
            int Class_Presentation = rs.getInt("Class_Presentation");
            int Class_Total = rs.getInt("Class_Total");
            String ID_Prova = rs.getString("ID_Prova");
            String ID_Juri = rs.getString("ID_Juri");
            // print the results
            
            System.out.format("Content: %s\nVisual: %s\nPresentation: %s\nTotal: %s\n", Class_Content, Class_Visual, 
                    Class_Presentation, Class_Total);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
    
    public void ConsultarDadosClassificacao(){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM classificacao";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String ID_Classificacao = rs.getString("ID_Classificacao");
            int Class_Content = rs.getInt("Class_Content");
            int Class_Visual = rs.getInt("Class_Visual");
            int Class_Presentation = rs.getInt("Class_Presentation");
            int Class_Total = rs.getInt("Class_Total");
            String ID_Prova = rs.getString("ID_Prova");
            String ID_Juri = rs.getString("ID_Juri");
            // print the results
            
            System.out.format("ID_Classificacao: %s\nClassificacao Content: %s\nClassificacao Visual: %s\nClassificacao Total: %s\n"
                    + "Classificacao Total: %s\nID_Prova: %s\nID_Juri: %s\n", ID_Classificacao, Class_Content, Class_Visual, 
                    Class_Presentation, Class_Total, ID_Prova, ID_Juri);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
}

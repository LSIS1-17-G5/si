/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.mysql.jdbc.Connection;
import model.Equipa;

/**
 *
 * @author Antonio
 */
public class EditarEquipaController {
    Connection conn;
    
    public EditarEquipaController(Connection connection){
        conn = connection;
    }
    
    public boolean AdicionarEquipa(String ID, String Nome){
        try{
        Equipa equipa = new Equipa(ID, Nome);
        Statement statement = (Statement) conn.createStatement();
        statement.executeUpdate("INSERT INTO equipa (NumeroEquipa, NomeEquipa) "
                    + "VALUES ('" + equipa.getM_ID() + "','" + equipa.getM_Nome() +"')");
        
        }catch(Exception e){
             System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean EditarEquipa(String ID, String Nome, String ID_Original){
        
        try{
            
            Equipa equipa = new Equipa(ID,Nome);
            
            String query = "UPDATE equipa SET NumeroEquipa=?, NomeEquipa=? WHERE NumeroEquipa=?";
            PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
            preparedStmt.setString(1, ID);
            preparedStmt.setString(2, Nome);
            preparedStmt.setString(3, ID_Original);
            
            preparedStmt.execute();
            
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public boolean EliminarEquipa(String ID){
        try{
            Statement statement = (Statement) conn.createStatement();
            statement.executeUpdate("DELETE FROM equipa WHERE NumeroEquipa=" + "'"+ID+"'");
        }catch(Exception e){
            System.out.println("\n*** Unable to commit ***\n");
            e.printStackTrace();
            return false;
        }
        
        System.out.println("Commited");
        return true;
    }
    
    public void ConsultarDadosEquipa_Esp(String ID_Equipa1){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM equipa WHERE NumeroEquipa= '" +ID_Equipa1+ "'";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String NumeroEquipa = rs.getString("NumeroEquipa");
            String NomeEquipa = rs.getString("NomeEquipa");
        
            // print the results
            
            System.out.format("Numero Equipa: %s\nNome Equipa:%s\n", NumeroEquipa, NomeEquipa);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
    
    public void ConsultarDadosEquipa(){
        try
        {
        // our SQL SELECT query. 
        // if you only need a few columns, specify them by name instead of using "*"
        String query = "SELECT * FROM equipa";

        // create the java statement
        Statement st = (Statement) conn.createStatement();
      
        // execute the query, and get a java resultset
        ResultSet rs = st.executeQuery(query);
      
        // iterate through the java resultset
        while (rs.next())
        {
            String NumeroEquipa = rs.getString("NumeroEquipa");
            String NomeEquipa = rs.getString("NomeEquipa");
        
            // print the results
            
            System.out.format("Numero Equipa: %s\nNome Equipa:%s\n", NumeroEquipa, NomeEquipa);
        }
        st.close();
        }
        catch (Exception e)
        {
        System.err.println("Got an exception! ");
        System.err.println(e.getMessage());
        }
    
    }
}
